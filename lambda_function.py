import json
import boto3

def lambda_handler(event, context):
   
    for record in event['Records']:
    
        jsondata = json.loads(record["body"])
        
        print('------------- PERSIST RESPONSE -------------')
        print('jsondata ', jsondata)
        print('--------------------------------------------')
        
        database = boto3.resource('dynamodb')
        table = database.Table("transaction-response")
        table.put_item(Item = jsondata)
   
    return {
        'statusCode': 200,
    }
